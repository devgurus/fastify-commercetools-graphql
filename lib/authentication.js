module.exports.getAccessToken = async keys => {
  const { clientId, clientSecret, oauthHost, scope } = keys;
  const tokenBase64 = Buffer.from(`${clientId}:${clientSecret}`).toString(
    "base64"
  );
  const res = await fetch(`${oauthHost}/oauth/token`, {
    method: "POST",
    body: `grant_type=client_credentials&scope=${scope}`,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Basic ${tokenBase64}`
    }
  });
  const data = await res.json();
  return data.access_token;
};
