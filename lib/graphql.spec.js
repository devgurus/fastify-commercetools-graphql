const { executeQuery } = require("./graphql");
const { getAccessToken } = require("./authentication");
jest.mock("./authentication");

describe("graphql", () => {
  describe("executeQuery", () => {
    let query, keys, jsonSpy, response;
    beforeEach(async () => {
      jsonSpy = jest.fn().mockReturnValue({ data: "response-foo" });
      global.fetch = jest.fn().mockReturnValue({ json: jsonSpy });
      query = "foo-query";
      keys = {
        host: "foo-host",
        projectKey: "foo-project"
      };
      getAccessToken.mockResolvedValue("foo-token");
      response = await executeQuery(query, keys);
    });
    it("should run fetch with correct values", () => {
      expect(fetch).toHaveBeenCalledWith("foo-host/foo-project/graphql", {
        body: '{"query":"foo-query"}',
        headers: {
          Authorization: "Bearer foo-token",
          "Content-Type": "application/json"
        },
        method: "POST"
      });
    });
    it("should run res.json", () => {
      expect(jsonSpy).toHaveBeenCalled();
    });
    it("should return the correct data", () => {
      expect(response.data).toEqual("response-foo");
    });
  });
});
