const fp = require("fastify-plugin");
const { executeQuery } = require("./graphql");

const plugin = (fastify, _, next) => {
  fastify.decorate("graphql", {
    createClient: keys => {
      const {
        host,
        projectKey,
        clientId,
        clientSecret,
        oauthHost,
        scope
      } = keys;

      if (!host) throw Error("Missing host.");
      if (!projectKey) throw Error("Missing projectKey.");
      if (!clientId) throw Error("Missing clientId.");
      if (!clientSecret) throw Error("Missing clientSecret.");
      if (!oauthHost) throw Error("Missing oauthHost.");
      if (!scope) throw Error("Missing scope.");

      return {
        executeQuery: query => executeQuery(query, keys)
      };
    }
  });
  next();
};

module.exports = fp(plugin, {
  name: "fastify-commercetools-graphql"
});
