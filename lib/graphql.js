const { getAccessToken } = require("./authentication");

module.exports.executeQuery = async (query, keys) => {
  const { host, projectKey } = keys;
  const res = await fetch(`${host}/${projectKey}/graphql`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${await getAccessToken(keys)}`
    },
    body: JSON.stringify({ query })
  });

  return res.json();
};
