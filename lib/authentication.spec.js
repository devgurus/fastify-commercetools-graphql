const { getAccessToken } = require("./authentication");

describe("authentication", () => {
  describe("getAccessToken", () => {
    let keys, jsonSpy, response;
    beforeEach(async () => {
      jsonSpy = jest.fn().mockReturnValue({ access_token: "foo-token" });
      global.fetch = jest.fn().mockReturnValue({ json: jsonSpy });
      keys = {
        clientId: "client-foo",
        clientSecret: "secret-foo",
        oauthHost: "foo-auth",
        scope: "foo-scope"
      };
      response = await getAccessToken(keys);
    });
    it("should run fetch with correct values", () => {
      expect(fetch).toHaveBeenCalledWith("foo-auth/oauth/token", {
        body: "grant_type=client_credentials&scope=foo-scope",
        headers: {
          Authorization: "Basic Y2xpZW50LWZvbzpzZWNyZXQtZm9v", //Base64
          "Content-Type": "application/x-www-form-urlencoded"
        },
        method: "POST"
      });
    });
    it("should run res.json", () => {
      expect(jsonSpy).toHaveBeenCalled();
    });
    it("should return the correct data", () => {
      expect(response).toEqual("foo-token");
    });
  });
});
