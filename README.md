# fastify-commercetools-graphql

commercetools graphql fastify plugin that decorate fastify to can make graphql querys to commercetools:

## Install

```
npm i fastify-commercetools-graphql --save
```

## Usage

### Register the plugin

Add it to your project with `register` and create client.

```js
const fastify = require("fastify")();

fastify.register(require("fastify-commercetools"), {
  commercetools: {
    host: "https://api.commercetools.co",
    oauthHost: "https://auth.commercetools.co",
    projectKey: "projectKey",
    clientId: "clientId",
    clientSecret: "clientSecret",
    concurrency: 5
  }
});
```

```js
module.exports = (fastify, _, done) => {
  const client = fastify.graphql.createClient({
    host: "https://api.commercetools.co",
    oauthHost: "https://auth.commercetools.co",
    projectKey: "projectKey",
    clientId: "clientId",
    clientSecret: "clientSecret",
    scope: "manage_project:projectKey"
  });
};
```

Once you create a client you can use the plugin to do the following:

## execute

Can perform graphql querys from commercetools, ej:

```js
const { stripIndent } = require("common-tags");

const getProductsInfoQuery = stripIndent`query {
  products(limit:500) {
    results {
      masterData {
        current {
          slug(locale: "en"),
          name(locale:"en"),
          masterVariant {
            images {
              url
            }
          }
        }
      }
    }
  }
}`;
```
